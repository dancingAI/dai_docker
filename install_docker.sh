#!/bin/bash
# install_docker.sh
#
# For JetPack 4.4 production release (L4T R32.4.3)
#

print("\nDepreciated script. Kept for archives.\n")

# if [ "$EUID" -ne 0 ]
#   then echo "Please run as root"
#   exit
# fi

# if [ ! -f /var/run/resume-after-reboot ]; then
#   echo "running script for the first time..."
#   ABSOLUTE_PATH="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)/$(basename "${BASH_SOURCE[0]}")"
#   script="bash $ABSOLUTE_PATH/install_docker.sh"

# 1) UNINSTALL OLDER VERSIONS
# echo " "
# echo "UNINSTALLING OLD DOCKER"
# echo " "
# sudo apt-get remove -y docker docker-engine docker.io containerd runc 
# nvidia-container-toolkit nvidia-docker2

# 2) SET UP REPOSITORY AND INSTALL DEPENDENCIES
# echo " "
# echo "SETTING UP REPOSITORY AND INSTALLING DEPENDENCIES"
# echo " "
# sudo apt-get update
# sudo apt-get install -y \
# 			apt-transport-https \
# 			ca-certificates \
# 			curl \
# 			gnupg-agent \
# 			software-properties-common

# curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -

# 3) ADD REPOSITORY

# sudo add-apt-repository \
# 	"deb [arch=arm64] https://download.docker.com/linux/ubuntu \
# 	$(lsb_release -cs) \
# 	stable"

# 4) INSTALL DOCKER ENGINE
# echo " "
# echo "INSTALLING DOCKER ENGINE"
# echo " "
# sudo apt-get update
# sudo apt-get install -y docker-ce docker-ce-cli containerd.io
# sudo apt autoremove -y

# 5) POST-INSTALLATION: How to use Docker as non-root user
# echo " "
# echo "SETTING UP GROUPS"
# echo " "
# sudo groupadd docker
# sudo usermod -aG docker $USER
# sudo chmod 666 /var/run/docker.sock

# MAKE DOCKER START ON STARTUP
# echo " "
# echo "MAKE DOCKER START ON STARTUP"
# echo " "
# sudo systemctl enable docker

# Reboot
# echo " "
# echo "REBOOTING..."
# echo " "
# echo "$script" >> ~/.zshrc
# sudo touch /var/run/resume-after-reboot
# sudo reboot

# else
# echo "resuming script after reboot..."
# sed -i '/bash/d' ~/.zshrc 
# sudo rm -f /var/run/resume-after-reboot

# 6) ADD NATIVE GPU SUPPORT
# echo " "
# echo "ADDING NATIVE GPU AND CUDA USPPORT"
# echo " "
# distribution=$(. /etc/os-release;echo $ID$VERSION_ID)
# curl -s -L https://nvidia.github.io/nvidia-docker/gpgkey | sudo apt-key add -
# curl -s -L https://nvidia.github.io/nvidia-docker/$distribution/nvidia-docker.list | sudo tee /etc/apt/sources.list.d/nvidia-docker.list

# sudo apt-get update && sudo apt-get install -y nvidia-container-toolkit
# sudo apt install -y nvidia-docker2
# sudo systemctl daemon-reload
# sudo systemctl restart docker

# 7) Modify default runtime to build images using Cuda:
# sudo apt-get install -y jq
# sudo chmod 666 /etc/docker/daemon.json
# jq '.+= {"default-runtime": "nvidia"}' /etc/docker/daemon.json &> tmp.json
# cp tmp.json /etc/docker/daemon.json && rm tmp.json
# sudo chmod 644 /etc/docker/daemon.json

# 8) Increase Swap Size (set by default to 2G) (env. 4G needed to build OpenCV)
# echo " "
# echo "MODIFYING SWAP SIZE TO 4 GB"
# echo " "
# STRING='mem=$((("${totalmem}" * 1 / "${NRDEVICES}") * 1024))'
# sudo chmod 777 /etc/systemd/nvzramconfig.sh
# sed -i 's,mem=$.*,'"$STRING"',g' /etc/systemd/nvzramconfig.sh
# sudo chmod 755 /etc/systemd/nvzramconfig.sh

# 9) Reboot
# echo " "
# echo "THE END. PLEASE REBOOT NOW."
# echo " "
# sudo reboot
# fi