#!/bin/bash
set -e

# setup ros2 environment
source "/opt/ros/$ROS_DISTRO/setup.bash"
source "/workspace/install/setup.bash"
export AMENT_PREFIX_PATH=/workspace/install/dai_main:$AMENT_PREFIX_PATH
export AMENT_PREFIX_PATH=/workspace/install/dai_pi:$AMENT_PREFIX_PATH

# ros2 run dai_main user_command_service.py

exec "$@"