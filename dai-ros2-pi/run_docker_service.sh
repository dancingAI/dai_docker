#!/bin/bash

if [[ $# -ne 1 ]] ; then
    echo 'Usage: '
    echo 'bash run_docker.sh <TAG>'
    echo
    exit 0
fi

CONTAINER_IMAGE=impactia/dai-ros2-pi:$1
CONTAINER_NAME=dai-ros2-srv

# deactivate wifi
# echo "Wi-Fi deactivated."

if [ "$(docker inspect -f '{{.State.Running}}' ${CONTAINER_NAME} 2>/dev/null)" ]; then
    docker stop $CONTAINER_NAME
    docker rm $CONTAINER_NAME
fi

echo
echo ">>>>>>>>>> Starting new container <<<<<<<<<<"
echo "Using: ${CONTAINER_IMAGE}"
echo    echo &&
docker run -d --network host -e DISPLAY=$DISPLAY \
    --name $CONTAINER_NAME \
    --restart always \
    --device=/dev/ttyAMA1:/dev/ttyAMA1 \
    --device=/dev/ttyS0:/dev/ttyS0 \
    $CONTAINER_IMAGE \
    ros2 run dai_main user_command_service.py


# reactivate wifi
# echo "Wi-Fi activated."