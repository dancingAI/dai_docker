#!/bin/bash

if [[ $# -ne 1 ]] ; then
    echo 'Usage: '
    echo 'bash run_docker.sh <TAG>'
    echo
    exit 0
fi

CONTAINER_IMAGE=impactia/dai-ros2-pi:$1
CONTAINER_NAME=dai-ros2

echo &&
echo ">>>>>>>>>> Starting new container <<<<<<<<<<"
echo "Using: ${CONTAINER_IMAGE}"
echo &&
docker run -it --rm --network host -e DISPLAY=$DISPLAY \
	--name $CONTAINER_NAME \
    --device=/dev/ttyAMA1:/dev/ttyAMA1 \
    --device=/dev/ttyS0:/dev/ttyS0 \
    $CONTAINER_IMAGE