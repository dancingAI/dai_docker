#   Dockerfile
# 
#   Installing ROS2 Eloquent and Gazebo 9 for Linux
# 

ARG BASE_IMAGE=gazebo:libgazebo9-bionic
FROM ${BASE_IMAGE}

ARG ROS_PKG=ros_base
ENV ROS_DISTRO=eloquent
ENV ROS_ROOT=/opt/ros/${ROS_DISTRO}

# ros domain id (1-255) must be the same on all devices
ENV ROS_DOMAIN_ID=88

ENV DEBIAN_FRONTEND=noninteractive

WORKDIR /workspace

# change the locale from POSIX to UTF-8
RUN apt-get update && apt-get install -y locales && rm -rf /var/lib/apt/lists/* \
    && locale-gen en_US en_US.UTF-8 && update-locale LC_ALL=en_US.UTF-8 LANG=en_US.UTF-8
ENV LANG=en_US.UTF-8
# ENV LANG=C.UTF-8

# add the ROS deb repo to the apt sources list
RUN apt-get update \
    && apt-get install -y --no-install-recommends \
        git \
		cmake \
		build-essential \
		curl \
		wget \ 
		gnupg2 \
		lsb-release \
        nano \
		vim \
    && rm -rf /var/lib/apt/lists/*
    
RUN wget --no-check-certificate https://raw.githubusercontent.com/ros/rosdistro/master/ros.asc \
    && apt-key add ros.asc
RUN sh -c 'echo "deb [arch=$(dpkg --print-architecture)] http://packages.ros.org/ros2/ubuntu $(lsb_release -cs) main" > /etc/apt/sources.list.d/ros2-latest.list'

# install ROS packages
RUN apt-get update \
    && apt-get install -y --no-install-recommends \
		ros-eloquent-desktop \
    && rm -rf /var/lib/apt/lists/*
  
# clone dai_ros_package repos and build ROS2 packages
# RUN git clone https://github.com/dusty-nv/ros_deep_learning.git /workspace/src/ros_deep_learning \
#     && git clone https://gitlab.com/dancingAI/dai_ros_package.git /workspace/src/dai_ros_package \
#     && cd /workspace/ \
#     && . ${ROS_ROOT}/setup.sh \
#     && colcon build \
#     && cd ..

# setup entrypoint
COPY ros_entrypoint.sh /ros_entrypoint.sh
RUN chmod +x /ros_entrypoint.sh 

# the end
WORKDIR /workspace
ENTRYPOINT ["/ros_entrypoint.sh"]
CMD [ "gzserver", "my-gazebo-app-args" ]