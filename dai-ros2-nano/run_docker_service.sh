#!/bin/bash

if [[ $# -ne 1 ]] ; then
    echo 'Usage: '
    echo 'bash run_docker_service.sh <TAG>'
    echo
    exit 0
fi

CONTAINER_IMAGE=impactia/dai-ros2-nano:$1
CONTAINER_NAME=dai-ros2-srv
MODELS_DIR=$(pwd)/networks

# START_CMD='. install/setup.sh && ros2 run dai_main user_command_service.py'

if [ "$(docker inspect -f '{{.State.Running}}' ${CONTAINER_NAME} 2>/dev/null)" ]; then
    docker stop $CONTAINER_NAME
    docker rm $CONTAINER_NAME
fi

echo
echo ">>>>>>>>>> Starting new container <<<<<<<<<<"
echo "Using: ${CONTAINER_IMAGE}"
echo
docker run -d --network host -e DISPLAY=$DISPLAY \
    --name $CONTAINER_NAME \
    --runtime nvidia \
    --restart always \
    -v /tmp/.X11-unix/:/tmp/.X11-unix \
    -v /tmp/argus_socket:/tmp/argus_socket \
    -v $MODELS_DIR:/jetson-inference/data/networks \
    --device=/dev/video0:/dev/video0 \
    --device=/dev/video1:/dev/video1 \
    $CONTAINER_IMAGE \
    ros2 run dai_main user_command_service.py