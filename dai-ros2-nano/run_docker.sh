#!/bin/bash

if [[ $# -ne 1 ]] ; then
    echo 'Usage: '
    echo 'bash run_docker.sh <TAG>'
    echo
    exit 0
fi

CONTAINER_IMAGE=impactia/dai-ros2-nano:$1
MODELS_DIR=$(pwd)/networks
CONTAINER_NAME=dai-ros2

echo &&
echo ">>>>>>>>>> Starting new container <<<<<<<<<<"
echo "Using: ${CONTAINER_IMAGE}"
echo &&
docker run -it --rm --network host -e DISPLAY=$DISPLAY \
	--name $CONTAINER_NAME \
	--runtime nvidia \
    -v /tmp/.X11-unix/:/tmp/.X11-unix \
    -v /tmp/argus_socket:/tmp/argus_socket \
    -v $MODELS_DIR:/jetson-inference/data/networks \
    --device=/dev/video0:/dev/video0 \
    --device=/dev/video1:/dev/video1 \
    $CONTAINER_IMAGE