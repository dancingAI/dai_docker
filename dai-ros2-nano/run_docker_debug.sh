#!/bin/bash

if [[ $# -ne 1 ]] ; then
    echo 'Usage: '
    echo 'bash run_docker_debug.sh <TAG>'
    echo
    exit 0
fi

CONTAINER_IMAGE=impactia/dai-ros2-nano:$1
MODELS_DIR=$(pwd)/networks
CONTAINER_NAME=dai-ros2-debug

echo &&
echo ">>>>>>>>>> Starting new debug container <<<<<<<<<<"
echo "Using: ${CONTAINER_IMAGE}"
echo &&
docker run -it --rm --network host -e DISPLAY=$DISPLAY \
	--name $CONTAINER_NAME \
	--runtime nvidia \
    -v /tmp/.X11-unix/:/tmp/.X11-unix \
    -v /tmp/argus_socket:/tmp/argus_socket \
    -v $MODELS_DIR:/jetson-inference/data/networks \
    --device=/dev/video0:/dev/video0 \
    --device=/dev/video1:/dev/video1 \
    --volume ~/dai_repos/dai_ros_packages:/workspace/src/dai_ros_packages \
    $CONTAINER_IMAGE


    # ros2 run dai_main user_command_service.py
    # --entrypoint=/ros_entrypoint.sh \
