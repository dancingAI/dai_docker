#!/bin/bash

if [[ $# -ne 1 ]] ; then
    echo 'Usage: '
    echo 'bash run_docker_client.sh <TAG>'
    echo
    exit 0
fi

CONTAINER_IMAGE=impactia/dai-ros2-ubuntu18.04:$1
CONTAINER_NAME=dai-ros2-cli

# START_CMD='. install/setup.sh && ros2 run dai_main user_command_client.py'

# deactivate wifi
# echo "Wi-Fi deactivated."

if [ "$(docker inspect -f '{{.State.Running}}' ${CONTAINER_NAME} 2>/dev/null)" ]; then
    docker stop $CONTAINER_NAME
    docker rm $CONTAINER_NAME
fi

echo
echo ">>>>>>>>>> Starting new container <<<<<<<<<<"
echo "Using: ${CONTAINER_IMAGE}"
echo
docker run -it --rm --network host -e DISPLAY=$DISPLAY \
    --name $CONTAINER_NAME \
    $CONTAINER_IMAGE \
    ros2 run dai_main user_command_client.py