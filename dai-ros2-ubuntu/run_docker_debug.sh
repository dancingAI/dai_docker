#!/bin/bash

if [[ $# -ne 1 ]] ; then
    echo 'Usage: '
    echo 'bash run_docker_debug.sh <TAG>'
    echo
    exit 0
fi

CONTAINER_IMAGE=impactia/dai-ros2-ubuntu18.04:$1
CONTAINER_NAME=dai-ros2-debug

xhost +si:localuser:root

if [ "$(docker inspect -f '{{.State.Running}}' ${CONTAINER_NAME} 2>/dev/null)" ]; then
    docker stop $CONTAINER_NAME
    docker rm $CONTAINER_NAME
fi

echo
echo ">>>>>>>>>> Starting new container <<<<<<<<<<"
echo "Using: ${CONTAINER_IMAGE}"
echo
docker run -it --rm --network host -e DISPLAY=$DISPLAY \
    --name $CONTAINER_NAME \
    --entrypoint='/bin/bash' \
    --volume=/home/valentin/Documents/dai_repos/dai_ros_packages:/workspace/src/dai_ros_packages \
    $CONTAINER_IMAGE \
    ros2 run rqt_gui rqt_gui