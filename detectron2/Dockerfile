# Dockerfile
# Using JetPack 4.4

FROM nvcr.io/nvidia/l4t-base:r32.4.3 AS OpenCV_base

# Avoid interactions during installation
ARG DEBIAN_FRONTEND=noninteractive

WORKDIR /app

# Set local timezone
ENV TZ=Europe/Zurich
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezon

# Install Essentials dependencies
RUN apt-get update && apt-get install -y --no-install-recommends \
    git build-essential v4l-utils sudo cmake \
    && rm -rf /var/lib/apt/lists/*

# Install Python dependencies
RUN apt-get update && apt-get install -y --no-install-recommends \
    python3-dev \
    python3-tk \
    python3-matplotlib \
    python3-pip \
    && rm -rf /var/lib/apt/lists/*
RUN pip3 install --upgrade pip setuptools wheel

# Install other dependencies
RUN pip3 install \
    tqdm \
    cython \
    pycocotools

#
# Build OpenCV 4.4.0 w/ cuda 10.2 from source
#
# from https://github.com/mdegans/nano_build_opencv
RUN git clone https://github.com/Kinchul/nano_build_opencv /app/opencv
WORKDIR /app/opencv
RUN ./build_opencv.sh 4.4.0


FROM OpenCV_base AS detectron2_base
#
# Install PyTorch 1.6.0
#
# from https://forums.developer.nvidia.com/t/pytorch-for-jetson-version-1-6-0-now-available/72048
WORKDIR /app/torch
RUN wget --no-verbose https://nvidia.box.com/shared/static/9eptse6jyly1ggt9axbja2yrmj6pbarc.whl \
    -O torch-1.6.0-cp36-cp36m-linux_aarch64.whl 
RUN pip3 install torch-1.6.0-cp36-cp36m-linux_aarch64.whl

#
# Install Torchvision 0.7.0
#
RUN apt-get update && apt-get install -y --no-install-recommends \
    libjpeg-dev zlib1g-dev openmpi-bin libopenblas-dev && \
    rm -rf /var/lib/apt/lists/*
RUN git clone --branch v0.7.0 https://github.com/pytorch/vision /app/torchvision
WORKDIR /app/torchvision
ARG BUILD_VERSION=0.7.0
RUN python3 setup.py install

#
# Install torch2trt
#
RUN git clone https://github.com/NVIDIA-AI-IOT/torch2trt /app/torch2trt
WORKDIR /app/torch2trt
RUN python3 setup.py install

#
# Setup Cuda environemnt variables
# 
ENV PATH="/usr/local/cuda/bin:${PATH}"
ENV LD_LIBRARY_PATH="/usr/local/cuda/lib64:${LD_LIBRARY_PATH}"

#
# Install and build jetson utils
#
RUN git clone https://github.com/dusty-nv/jetson-utils.git /app/jetson-utils
WORKDIR /app/jetson-utils
RUN mkdir build && cd build && cmake ../ && make install

FROM detectron2_base
#
# Install detectron2
#
RUN git clone https://github.com/facebookresearch/detectron2.git /app/detectron2
WORKDIR /app/detectron2
RUN rm -rf build/ **/*.so
RUN pip3 install -e ../detectron2


WORKDIR /app

### the end
CMD ["/bin/bash"]