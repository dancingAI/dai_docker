#!/bin/bash

if [[ $# -ne 1 ]] ; then
    echo 'Usage: '
    echo 'bash run_docker.sh <TAG>'
    echo
    exit 0
fi

CONTAINER_IMAGE=impactia/dai-ros2-xavier:$1
CONTAINER_NAME=dai-ros2

# setup main control dir
MAIN_CONTROL_DOCKER_DIR=/workspace/dai_main_control

xhost +si:localuser:root

POZYX_PORT=$(python3 -c "from pypozyx import *; print(get_first_pozyx_serial_port())")

if [ $POZYX_PORT = "None" ]; then
    echo 'Error: Could not find Pozyx port.'
    exit 1
else
    echo "Pozyx port: $POZYX_PORT"
fi

echo &&
echo ">>>>>>>>>> Starting new container <<<<<<<<<<"
echo "Using: ${CONTAINER_IMAGE}"
echo &&
docker run -it --rm --network host -e DISPLAY=$DISPLAY \
    --name $CONTAINER_NAME \
    --runtime nvidia \
    --device=$POZYX_PORT:$POZYX_PORT \
    --env DAI_MAIN_CONTROL_DIR=${MAIN_CONTROL_DOCKER_DIR} \
    --volume ~/dai_repos/dai_main_control:${MAIN_CONTROL_DOCKER_DIR} \
    $CONTAINER_IMAGE
    # ros2 run rqt_gui rqt_gui
