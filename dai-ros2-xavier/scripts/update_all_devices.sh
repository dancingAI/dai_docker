#!/bin/bash
# 
# --------- AUTO UPDATE SCRIPT -------------
# 
# /!\ meant to be ran from Dai's Jetson Xavier /!\
# 
# Update cloned dai repos on all devices
# Builds latest Docker image on all devices
# 
# Note: if connected with ssh to the Xavier, will update one device at a time.
# 
# ##########################################
# 
# https://stackoverflow.com/questions/17403498/iterate-over-two-arrays-simultaneously-in-bash
# https://stackoverflow.com/questions/22107610/shell-script-run-function-from-script-over-ssh
# https://unix.stackexchange.com/questions/103920/parallelize-a-bash-for-loop
# 
# ##########################################


if [[ $# -ne 1 ]] ; then
    echo 'Usage: '
    echo 'bash update_all_devices.sh <TAG>'
    echo
    exit 0
fi

DEVICE_IPS=( "192.168.1.100" "192.168.1.101" "192.168.1.102" "192.168.1.103" "192.168.1.111" "192.168.1.112" "192.168.1.113" )
DEVICE_TYPES=( "xavier" "nano" "nano" "nano" "pi" "pi" "pi")
NEW_VERSION=$1

update () {
  device=$1
  new_version=$2

  # Updates git repos
  cd ~/dai_repos
  for entry in *; do
      echo "- Updating $entry :"
      cd $entry
      git fetch --all
      if [ $entry = "dai_main_control" ]; then
        git reset --hard origin/ros_dev
      else
        git reset --hard origin/master
      fi
      cd ..
      echo
  done

  # Updates docker container
  # todo: update to latest : problems if there are multiple versions
  echo "- Updating Docker:"
  local is_installed=`docker images impactia/dai-ros2-$device | grep $new_version`
  if [ -z "$is_installed" ]; then
    docker stop dai-ros2
    docker rm dai-ros2
    cd ~/dai_repos/dai_docker/dai-ros2-$device
    echo "Building docker image: dai-ros2-$device:$new_version"
    bash build_docker.sh $new_version
    echo
    echo "Docker image updated to: dai-ros2-$device:$new_version"
    docker stop dai-ros2 dai-ros2-srv dai-ros2-cli
    docker rm dai-ros2 dai-ros2-srv dai-ros2-cli
    bash run_docker_service.sh $new_version
  else
    echo "Docker image alrady up-to-date: dai-ros2-$device:$new_version"
  fi
}

ssh_update () {
  ip=$1
  device=$2
  new_version=$3

  if [ $device = "pi" ]; then
    ssh pi@$ip "$(typeset -f update); update $device $new_version"
  elif [ $device = "nano" ]; then
    ssh dai@$ip "$(typeset -f update); update $device $new_version"
  fi
}

# make update functions available to child terminals
export -f update
export -f ssh_update

# updating all devices
for i in "${!DEVICE_IPS[@]}"; do

  if [ ${DEVICE_TYPES[i]} = "xavier" ]; then
    echo "Started to update ${DEVICE_IPS[i]} (${DEVICE_TYPES[i]}) to $NEW_VERSION"
    if xhost >& /dev/null ; then gnome-terminal -- bash -c "update ${DEVICE_TYPES[i]} $NEW_VERSION; bash" &
    else update ${DEVICE_TYPES[i]} $NEW_VERSION
    fi
  else
    echo "Started to update ${DEVICE_IPS[i]} (${DEVICE_TYPES[i]}) to $NEW_VERSION"
    if xhost >& /dev/null ; then gnome-terminal -- bash -c "ssh_update ${DEVICE_IPS[i]} ${DEVICE_TYPES[i]} $NEW_VERSION; bash" &
    else ssh_update ${DEVICE_IPS[i]} ${DEVICE_TYPES[i]} $NEW_VERSION
    fi
  fi
  echo

done