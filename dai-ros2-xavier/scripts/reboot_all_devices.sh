#!/bin/bash
# 
# --------- AUTO REBOOT SCRIPT -------------
# 
# /!\ meant to be ran from Dai's Jetson Xavier /!\
# 
# Reboot all Nanos and Pis.
# Uses SSH with key authentication.
# 
# Must run:
#   $ sudo chmod u+s /sbin/shutdown
# on all hosts once to allow shutdown from non root users.
# 
# ##########################################

DEVICE_IPS=( "192.168.1.101" "192.168.1.102" "192.168.1.103" "192.168.1.111" "192.168.1.112" "192.168.1.113" )
DEVICE_TYPES=( "nano" "nano" "nano" "pi" "pi" "pi")

reboot () {
  /sbin/shutdown -r now
}

# iterating over all devices
echo "Rebooting all devices:"
for i in "${!DEVICE_IPS[@]}"; do

  if [ ${DEVICE_TYPES[i]} = "pi" ]; then
    ssh pi@${DEVICE_IPS[i]} "$(typeset -f reboot); reboot"

  elif [ ${DEVICE_TYPES[i]} = "nano" ]; then
    ssh dai@${DEVICE_IPS[i]} "$(typeset -f reboot); reboot"

  fi
done

echo "Waiting for devices to boot... (15 sec)"
sleep 15

echo "Trying to reconnect:"
for i in "${!DEVICE_IPS[@]}"; do
    echo -n "${DEVICE_IPS[i]} is " && ping -c 2 ${DEVICE_IPS[i]} > /dev/null && echo "up" || echo "down"
done

read -p "Do you want to reboot the Xavier ? (y/n)" choice
if [ "$choice" = "y" ]; then
  shutdown -r now;
else
  echo "no";
fi