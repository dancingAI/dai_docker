#!/bin/bash
# 
# --------- AUTO SHUTDOWN SCRIPT -------------
# 
# /!\ meant to be ran from Dai's Jetson Xavier /!\
# 
# Shutting down all Nanos and Pis.
# Uses SSH with key authentication.
# 
# Must run:
#   $ sudo chmod u+s /sbin/shutdown
# on all hosts once to allow shutdown from non root users.
# 
# ##########################################

DEVICE_IPS=( "192.168.1.101" "192.168.1.102" "192.168.1.103" "192.168.1.111" "192.168.1.112" "192.168.1.113" )
DEVICE_TYPES=( "nano" "nano" "nano" "pi" "pi" "pi")

shutdown () {
  /sbin/shutdown -h now
}

# iterating over all devices
echo "Shutting down all devices:"
for i in "${!DEVICE_IPS[@]}"; do

  if [ ${DEVICE_TYPES[i]} = "pi" ]; then
    ssh pi@${DEVICE_IPS[i]} "$(typeset -f shutdown); shutdown"

  elif [ ${DEVICE_TYPES[i]} = "nano" ]; then
    ssh dai@${DEVICE_IPS[i]} "$(typeset -f shutdown); shutdown"

  fi
done

read -p "Do you want to shutdown the Xavier ? (y/n)" choice
if [ "$choice" = "y" ]; then
  shutdown -h now;
else
  echo "no";
fi