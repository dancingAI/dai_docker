#!/bin/bash

if [[ $# -ne 1 ]] ; then
    echo 'Usage: '
    echo 'bash build_docker.sh <TAG>'
    echo
    exit 0
fi

# close and rm previous images
docker stop dai-ros2
docker rm dai-ros2

# build image
docker build --build-arg CACHEBUST=$(date +%s) -t impactia/dai-ros2-xavier:$1 .

export LATEST_DAI_ROS2_BUILD=$1